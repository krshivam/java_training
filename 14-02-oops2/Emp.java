class Emp
{
	String name;
	int salary;
	
	void get (String s1, int s2)
	{
		name = s1;
		salary = s2;
	}
	
	void show()
	{
		System.out.println( name );
		System.out.println( salary );
	}
	
	public static void main (String[] s)
	{
		
		Emp e1 = new Emp();
		e1.get("Lalu" , 101);
		e1.show();
		
		Emp e2 = new Emp();
		e2.get("Rabri" , 51);
		e2.show();
	}
}

/**
*   OUTPUT:

Lalu
101
Rabri
51
*/