class Emp
{
	String name;
	int salary;
	
	void get (String s1, int s2)
	{
		name = s1;
		salary = s2;
	}
	
	void show()
	{
		System.out.println( name );
		System.out.println( salary );
	}
	
	public static void main (String[] s)
	{
		
		Emp e1 = new Emp();
		e1.name = "Lalu";
		e1.salary = 101;
		System.out.println( e1 );
		
		// Another ref var e2 holds same REF ID
		Emp e2 = e1;
		System.out.println( e2 );
		
		// Change data member using e2
		e2.name = "Bhalu";
		
		// Print data member using e1.
		// It will still reflect the change done using e2 bcoz they refer to same object
		e1.show();
	}
}

/**
*   OUTPUT:

Emp@1db9742
Emp@1db9742
Bhalu
101

*/