class DoubleLiteral
{
	// float f = 5.6	; 
	/* this will produce a compile time error
	 * error: incompatible types: possible lossy conversion from double to float */
	// float a = 5.6f;
	 // will not produce any error
	 
	void show(float f)
	{
		System.out.println("From show(float)");
		System.out.println(f);
	}
	void show(double f)
	{
		System.out.println("From show(double)");
		System.out.println(f);
	}
	
	public static void main(String[] s)
	{
		DoubleLiteral d = new DoubleLiteral();
		d.show(2.4);	// default is double
		d.show(5.6f);
	}
}

/**
*	OUTPUT :

From show(double)
2.4
From show(float)
5.6

*/