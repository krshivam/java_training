/**
* TypeCasting
*/
class TypeCast
{
	public static void main (String[] s)
	{
		// Implicit typecasting or widening
		byte b = 10;
		int x = b;
		// Explicit typecasting or narrowing
		int y = 10;
		byte c = (byte) y;
		
		System.out.println(x);
		System.out.println(c);
	}
}

/**
*	OUTPUT:

10
10

*/