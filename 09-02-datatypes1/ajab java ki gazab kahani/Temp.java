class Temp
{
	public static void main (String[] s)
	{
		// This  will throw an error
		float z = 2.4;
		
		// This is all right
		byte b = 10;
		
		System.out.println( z );
		System.out.println( b );
	}
}

/**
*	OUTPUT :

 6 : error: incompatible types: possible lossy conversion from double to float


*/