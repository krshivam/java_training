/**
* By default, all integer literals in Java are treated as 'int' in Java.
*/
class IntegerLiteral
{
	public static void main (String[] s)
	{
		byte b = 10;
		byte c = 20;
		/* Next line will produce compilation error	*/
		/* error: incompatible types: possible lossy conversion from int to byte */
		// byte d = b + c;
		/* REsult of b + c is treated as an integer literal and 
		 * it cannot be assigned to a byte type variable. */
		 
		/* This does not produce a compilation error*/
		byte d = (byte) (b + c);
		System.out.println(d);
	}
}

/**
*	OUTPUT:

*	byte d = b + c;

error: incompatible types: possible lossy conversion from int to byte

*  byte d = (byte)(b + c);
30

*/