/**
Every data type in Java are having a default value.
This rule applies to only class level variables.
*/

class DefaultValues {
	byte by; 		/* 0	*/
	short sh;		/* 0	*/
	int x;			/* 0	*/
	long ly;		/* 0	*/
	char ab;		/* /u0000	*/
	boolean bo;		/* false	*/
	float flo;		/* 0.0f	*/
	double dou;		/* 0.0d	*/
	
	void showDefault()
	{
		System.out.println(by);
		System.out.println(sh);
		System.out.println(x);
		System.out.println(ly);
		
		System.out.println(String.format("\\u%04x", (int) ab));
		System.out.println(bo);
		System.out.println(flo + "f");
		System.out.println(dou + "d");
	}
	public static void main (String[] s)
	{
		DefaultValues d = new DefaultValues();
		d.showDefault();
	}
}

/**
OUTPUT:

0
0
0
0
\u0000
false
0.0f
0.0d

*/
