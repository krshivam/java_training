/**
Local variables don't have a default value.
*/

class Demo
{
	int x = 10;
	
	void show()
	{
		int v;
		// This will throw a compilation error because 
		// local variables don't have a default value.
		// local variables must be initialized before the first use.
		System.out.println( v );
	}
	public static void main (String[] s)
	{
		Demo d = new Demo();
		d.show();
	}
}

/**
OUTPUT:

error: variable v might not have been initialized
                System.out.println( v );

*/
