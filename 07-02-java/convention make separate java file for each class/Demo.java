/**
* Convention: Make separate Java file for each class.
*/
class Demo
{
	static public void main(String... s)
	{
		System.out.println("Always make separate Java files for each class..");
	}
}

/**
* OUTPUT :
* java Demo

Always make separate Java files for each class..

*/