/**
* Bytecode of every class is stored in a separate .class file and
* the name of that class file is same as your class name.
*/
class Demo
{
	static public void main(String... s)
	{
		System.out.println("Hello from Class Demo's main");
	}
}

class Demo1
{
	public static void main(String... s){
		System.out.println("Calling Demo's main function");
		Demo.main(s);
		System.out.println("Hello from Class Demo1's main");
	}
}

/**
* OUTPUT :
* java Demo

Hello from Class Demo's main

* java Demo1

Calling Demo's main function
Hello from Class Demo's main
Hello from Class Demo1's main

*/