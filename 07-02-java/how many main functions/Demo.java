class Demo
{
	public static void main(String... s)
	{
		System.out.println("Only this main will run by default");
	}
	void main()
	{
		System.out.println("Another main function");
	}
	int main(int x)
	{
		System.out.println("Again another main function");
		return x;
	}
}

/**
* OUTPUT :
* java Demo

Only this main will run by default

*/