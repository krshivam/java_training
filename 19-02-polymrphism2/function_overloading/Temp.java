class Temp
{
	
	
	void show()
	{
		System.out.println("Function overloading rocks!!!");
	}
	
	void show(int x)
	{
		System.out.println("\nGot integer x");
		System.out.println( x );
	}
	
	void show(char ch)
	{
		System.out.println("\nGot Character ch");
		System.out.println( ch );
	}
	
	void show(int x, int y)
	{
		System.out.println("\nGot integer x and integer y");
		System.out.println( x );
		System.out.println( y );
	}
	
	void show(int x, char y, float z)
	{
		System.out.println("\nGot integer x , Character y and Float z");
		System.out.println( x );
		System.out.println( y );
		System.out.println( z );
	}
	
	public static void main(String[] s)
	{
		
		Temp t = new Temp();
		t.show();		
		t.show(10);
		t.show('a');
		t.show(20, 'D', 5.25f);
	}
}

/**
* OUTPUT:
Got integer x
10

Got Character ch
a

Got integer x , Character y and Float z
20
D
5.25

*/
