class Temp
{
	
	void show(int x, long y)
	{
		System.out.println("x:" + x);
		System.out.println("y:" + y);
	}
	
	void show(long y, int x)
	{
		System.out.println("y:" + y);
		System.out.println("x:" + x);
	}
	
	public static void main(String[] s)
	{
		
		Temp t = new Temp();
		t.show(10, 10l);
		t.show(10l, 10);
		// This should throw an ambiguity error
		t.show(10, 10);
	}
}

/**
* OUTPUT:
 error: reference to show is ambiguous
                t.show(10, 10);
                 ^
  both method show(int,long) in Temp and method show(long,int) in Temp match

*/
