class Emp
{
	String name;
	int salary;
	
	void get (String s1, int s2)
	{
		name = s1;
		salary = s2;
	}
	
	void show()
	{
		System.out.println( name );
		System.out.println( salary );
	}
	
	public static void main (String[] s)
	{
		String ename = "Lalu";
		int esalary = 101;
		
		Emp e = new Emp();
		e.get(ename , esalary);
		// prints CLASSNAME@<hex rep of hashcode of object>
		System.out.println(e);
	}
}

/**
*   OUTPUT:

Emp@1db9742

*/