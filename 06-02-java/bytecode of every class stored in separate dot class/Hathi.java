/**
* Bytecode of every class is stored in a separate .class file and
* the name of that class file is same as your class name.
*/
class Demo
{
	public static void main(String... s)
	{
		System.out.println("Hello from Class Demo's main");
	}
}

class Demo1
{
	public static void main(String... s){
		System.out.println("Hello from Class Demo1's main");
	}
}

/**
* OUTPUT :
* java Demo

Hello from Class Demo's main

* java Demo1

Hello from Class Demo1's main

*/