/**
* Demo class
*/
class Demo
{
	public static void main(String... s)
	{
		System.out.println("Hello from Java");
	}
}

/**
* OUTPUT :
* java Hathi

Error: Could not find or load main class Hathi

* java Demo

Hello from Java

*/