class Temp
{
	
	public static void main(String[] s)
	{
		// How come System.out.println can take ANY type of argument???
		// Bacause of polymorphism - function overloading
		
		System.out.println(10);
		System.out.println(5.4f);
		System.out.println("abcd");
		System.out.println('a');
		System.out.println(3.1416d);
		
	}
}

/**
OUTPUT:

*/