class Temp
{
	// How does this non-static 'x' get initialized ???
	int x = 10;
	// Whenever a new instance of Temp is created in heap, the constructor initializes this non static 'x'.
	// After that only, it returns the REF ID of the instance.
	
	void show (int x, Temp z)
	{
		System.out.println( x );
		System.out.println( z.x );
	}
	public static void main (String[] s)
	{
		Temp t = new Temp();
		t.show(20, t);
	}
}

/*
OUTPUT is :

20
10

because referenceID of an instance of Temp is passed as 
argument 'z' and z.x = t.x (in main) is the Class Level Variable x .
*/