class Temp
{
	// How does this non-static 'x' get initialized ???
	int x = 10;
	// Whenever a new instance of Temp is created in heap, the constructor initializes this non static 'x'.
	// After that only, it returns the REF ID of the instance.
	
	void show (int x)
	{
		System.out.println( x );
		System.out.println( x );
	}
	public static void main (String[] s)
	{
		Temp t = new Temp();
		t.show(20);
	}
}

/*
OUTPUT is :

20
20

because priority always goes to a local variable.
*/