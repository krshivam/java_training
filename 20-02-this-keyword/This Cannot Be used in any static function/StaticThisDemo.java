class StaticThisDemo
{
	public static void main (String[] s)
	{
		System.out.println("OK");
		// 'this' is unreachable in any static function because
		// static function don't need an instance to run
		// and 'this' always holds the REF ID of current object. 
		// Hence 'this' is a non-static variable and non-static variables cannot be referenced from
		// a static context.
		System.out.println(this);
		// so this should give compilation error.
	}
}

/*
OUTPUT - Java 1.8.0_162

 error: non-static variable this cannot be referenced from a static context
 
OUTPUT - Java 1.7.0_80
 same error

*/
